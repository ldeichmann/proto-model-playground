#!/bin/bash

projectId=${CI_PROJECT_ID}
token=${CI_JOB_TOKEN}

registryUrl="git@gitlab.com:ldeichmann/protosdc-rs-registry.git"
registryName="protosdc-rs-registry"

cd rust || exit 1
cargo build || exit 1

# deploy crates only if enabled
if [[ "$*" == *--deploy* ]]
then

# do setup for deployment, i.e. add ssh key and get version
if [[ -z "${CI}" ]]; then
  eval `ssh-agent -s`
  # add key
  echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - || exit 1
  # add trusted hosts
  ${SSH_TRUSTED_HOSTS} >> ~/.ssh/known_hosts
fi
crateVersion=$(python ./prepare_build.py)
ret=$?
if [ $ret -ne 0 ]; then
  exit 1
fi

# clone the registry repo to add crates later
git clone $registryUrl || exit 1
if [[ -z "${CI}" ]]; then
  # only update user in ci
  git config --global user.name "Lukas Deichmann Bot" || exit 1
  git config --global user.email "lukasdeichmann+gitlabbot@gmail.com" || exit 1
fi

# build and deploy proto and biceps-model
cd proto || exit 1
cargo package --allow-dirty || exit 1

cd ../biceps-model || exit 1
cargo package --allow-dirty || exit 1

cd .. || exit 1
curl --header "JOB-TOKEN: $token" \
     --upload-file target/package/proto-"$crateVersion".crate \
     "https://gitlab.com/api/v4/projects/$projectId/packages/generic/proto/$crateVersion/proto-$crateVersion.crate?status=default"

curl --header "JOB-TOKEN: $token" \
     --upload-file target/package/biceps-model-"$crateVersion".crate \
     "https://gitlab.com/api/v4/projects/$projectId/packages/generic/biceps-model/$crateVersion/biceps-model-$crateVersion.crate?status=default"

protoChecksum=$(sha256sum target/package/proto-"$crateVersion".crate | cut -d " " -f 1 )
bicepsChecksum=$(sha256sum target/package/biceps-model-"$crateVersion".crate | cut -d " " -f 1 )

# add proto and biceps-model to registry
cd $registryName || exit 1
./add_version.py --crate proto --crate-version "$crateVersion" --crate-checksum "$protoChecksum" --tonic-version 0.5 --prost-version 0.8 --walkdir-version 2.3 >> pr/ot/proto  || exit 1
git add pr/ot/proto || exit 1
git commit -m "add proto $crateVersion" || exit 1
./add_version.py --crate biceps-model --crate-version "$crateVersion" --crate-checksum "$bicepsChecksum" >> bi/ce/biceps-model  || exit 1
git add bi/ce/biceps-model || exit 1
git commit -m "add biceps-model $crateVersion" || exit 1
git push || exit 1

# now do the same for the mappers
cd ../rust-mappers || exit 1

cargo package --allow-dirty || exit 1
cd .. || exit 1

curl --header "JOB-TOKEN: $token" \
     --upload-file target/package/rust-mappers-"$crateVersion".crate \
     "https://gitlab.com/api/v4/projects/$projectId/packages/generic/rust-mappers/$crateVersion/rust-mappers-$crateVersion.crate?status=default"

mappersChecksum=$(sha256sum target/package/rust-mappers-"$crateVersion".crate | cut -d " " -f 1 )

cd $registryName || exit 1
./add_version.py --crate rust-mappers --crate-version "$crateVersion" --crate-checksum "$mappersChecksum" >> ru/st/rust-mappers || exit 1
git add ru/st/rust-mappers || exit 1
git commit -m "add rust-mappers $crateVersion" || exit 1
git push || exit 1

fi
