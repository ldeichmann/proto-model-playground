plugins {
    id("org.somda.sdc.protosdc_model.kotlin_library")
    id("org.somda.sdc.protosdc_model.grpc_versions")
    id("org.somda.sdc.protosdc_model.deploy")
}

var converterVersion: String by rootProject.extra
val protocVersion: String by extra

dependencies {
    // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-api-kotlin
    implementation(group = "org.apache.logging.log4j", name = "log4j-api-kotlin", version = "1.0.0")

    // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-api-kotlin
    implementation(group = "org.apache.logging.log4j", name = "log4j-core", version = "2.14.1")

    // https://mvnrepository.com/artifact/com.google.protobuf/protobuf-java
    implementation(group = "com.google.protobuf", name = "protobuf-java", version = protocVersion)

    implementation(group = "org.somda.sdc.protosdc_converter", name = "kotlin-proto-base-mappers", version = converterVersion)
    implementation(project(":biceps-model-kt"))
    implementation(project(":protosdc-model-kt"))
}

tasks {
    val sourcesJar by creating(Jar::class) {
        archiveClassifier.set("sources")
        from(sourceSets.main.get().allSource)
    }

    val javadocJar by creating(Jar::class) {
        dependsOn.add(javadoc)
        archiveClassifier.set("javadoc")
        from(javadoc)
    }

    artifacts {
        archives(sourcesJar)
        archives(javadocJar)
        archives(jar)
    }
}