#!/usr/bin/env python3

import os
import re

base_dir = os.path.dirname(os.path.abspath(__file__))

build = os.getenv("CI_PIPELINE_IID")
with open(os.path.join(base_dir, "..", "config", "base_version.txt")) as f:
    base_version = f.read()

grpc_versions = {}
with open(os.path.join(base_dir, "..", "config", "grpc_version.txt")) as f:
    for line in f:
        name, var = line.partition("=")[::2]
        grpc_versions[name.strip()] = var

with open(os.path.join(base_dir, "..", "config", "protobuf_version.txt")) as f:
    protobuf_version = f.read()


version = base_version
if build:
    version += "-alpha." + build

if not version:
    raise Exception("Version must be present")

def replace_version(data, new_version):
    out = ""
    for line in data.splitlines():
        # package version itself or dependencies
        if line.strip().startswith(("version = ", "biceps-model =", "proto =")):
            out += re.sub("version = \"[\d|a-zA-Z|.]*\"", "version = \"{}\"".format(version), line) + "\n"
        else:
            out += line + "\n"
    return out

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Prepare rust package versions")
    parser.add_argument("--version", help="Version to set to", type=str)
    args = parser.parse_args()

    targets = ["biceps-model/Cargo.toml", "proto/Cargo.toml", "rust-mappers/Cargo.toml"]

    for target in targets:
        with open(os.path.join(base_dir, target), "r+") as f:
            content = replace_version(f.read(), args.version or version)
            f.seek(0)
            f.write(content)
            f.truncate()

    print(version)