pub mod common {
    // used because of intellij completion
    include!(concat!(env!("OUT_DIR"), "/org.somda.protosdc.proto.model.common.rs"));
    // tonic::include_proto!("org.somda.protosdc.proto.model.common");
}

pub mod biceps {
    // used because of intellij completion
    include!(concat!(env!("OUT_DIR"), "/org.somda.protosdc.proto.model.biceps.rs"));
    // tonic::include_proto!("org.somda.protosdc.proto.model.biceps");
}

pub mod addressing {
    // used because of intellij completion
    include!(concat!(env!("OUT_DIR"), "/org.somda.protosdc.proto.model.addressing.rs"));
    // tonic::include_proto!("org.somda.protosdc.proto.model.addressing");
}

pub mod metadata {
    include!(concat!(env!("OUT_DIR"), "/org.somda.protosdc.proto.model.metadata.rs"));
}

#[allow(unused_imports)]
pub mod discovery {
    use super::addressing;
    use super::common;

    // used because of intellij completion
    include!(concat!(env!("OUT_DIR"), "/org.somda.protosdc.proto.model.discovery.rs"));
    // tonic::include_proto!("org.somda.protosdc.proto.model.discovery");
}

#[allow(unused_imports)]
pub mod sdc {
    use super::addressing;
    use super::biceps;

    // used because of intellij completion
    include!(concat!(env!("OUT_DIR"), "/org.somda.protosdc.proto.model.rs"));
    // tonic::include_proto!("org.somda.protosdc.proto.model");
}
