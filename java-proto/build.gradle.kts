import com.google.protobuf.gradle.*

plugins {
    java

    id("org.somda.sdc.protosdc_model.grpc_versions")
    id("org.somda.sdc.protosdc_model.deploy")
    id("com.google.osdetector")
}

// get the versions in here
val protocVersion: String by extra
val grpcJavaVersion: String by extra

dependencies {
    implementation(group = "com.google.protobuf", name = "protobuf-java", version = protocVersion)
    implementation(group = "io.grpc", name = "grpc-stub", version = grpcJavaVersion)
    implementation(group = "io.grpc", name = "grpc-protobuf", version = grpcJavaVersion)
    if (JavaVersion.current().isJava9Compatible) {
        // Workaround for @javax.annotation.Generated
        // see: https://github.com/grpc/grpc-java/issues/3633
        implementation("javax.annotation:javax.annotation-api:1.3.2")
    }

}

// https://github.com/grpc/grpc-java/issues/7690
val classified: String = when (val it = osdetector.classifier) {
    "osx-aarch_64" -> "osx-x86_64"
    else -> it
}

protobuf {
    protoc {
        // The artifact spec for the Protobuf Compiler
        artifact = "com.google.protobuf:protoc:$protocVersion:$classified"
    }
    plugins {
        // Optional: an artifact spec for a protoc plugin, with "grpc" as
        // the identifier, which can be referred to in the "plugins"
        // container of the "generateProtoTasks" closure.
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:$grpcJavaVersion:$classified"
        }
    }
    generateProtoTasks {
        ofSourceSet("main").forEach {
            it.plugins {
                // Apply the "grpc" plugin whose spec is defined above, without options.
                id("grpc")
            }
        }
    }
}