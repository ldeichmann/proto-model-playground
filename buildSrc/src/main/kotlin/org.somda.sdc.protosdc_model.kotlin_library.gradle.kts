plugins {
    `java-library`

    // version defined by buildSrc/build.gradle.kts dependencies
    kotlin("jvm")
}

repositories {
    gradlePluginPortal()
}

