import java.io.File
import java.util.*

plugins {
    // version defined by buildSrc/build.gradle.kts dependencies
    id("com.google.protobuf")
}

val grpcVersionProps = Properties()
grpcVersionProps.load(File(rootProject.projectDir, "./config/grpc_version.txt").inputStream())

// protobuf compiler version
val protocVersion by extra(File(rootProject.projectDir, "./config/protobuf_version.txt").readText())
// grpc versions
var grpcJavaVersion by extra(grpcVersionProps["grpc_java_version"])
var grpcKotlinVersion by extra(grpcVersionProps["grpc_kotlin_version"])
