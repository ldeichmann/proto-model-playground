plugins {
    `kotlin-dsl`
}

repositories {
    gradlePluginPortal()
}

dependencies {
    // actually depend on the plugin to make it available:
    implementation(plugin("com.google.protobuf", version = "0.8.15"))

    // actually depend on the plugin to make it available:
    implementation(plugin("org.jetbrains.kotlin.jvm", version = "1.4.31"))
}

// just a helper to get a syntax similar to the plugins {} block:
fun plugin(id: String, version: String) = "$id:$id.gradle.plugin:$version"
