plugins {
    id("org.somda.sdc.protosdc_model.kotlin_library")
    id("org.somda.sdc.protosdc_model.deploy")
}

// override group in subproject?
//group = "org.somda.sdc.biceps.model"

tasks {
    val sourcesJar by creating(Jar::class) {
        archiveClassifier.set("sources")
        from(sourceSets.main.get().allSource)
    }

    val javadocJar by creating(Jar::class) {
        dependsOn.add(javadoc)
        archiveClassifier.set("javadoc")
        from(javadoc)
    }

    artifacts {
        archives(sourcesJar)
        archives(javadocJar)
        archives(jar)
    }
}