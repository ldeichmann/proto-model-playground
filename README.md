# ProtoSDC Model

## Discovery model

Defined below are the basic definitions for the terms used in the discovery model of ProtoSDC.

**Endpoint:** A network peer in an IP-based IT network. It is accessible at one or more physical addresses.

**Endpoint identifier:** A stable, globally unique identifier of an *Endpoint* that is constant across re-initializations of the *Endpoint*, and constant across network interfaces and IPv4/v6.

**Service Provider:** An *Endpoint* that makes itself available for discovery.

**Service Consumer:** An *Endpoint* that searches for *Service Providers*.

**Discovery Proxy:** An *Endpoint* that facilitates discovery of *Service Providers* by *Service Consumers*.

**Scope:**  A message attribute to be used by *Service Providers* to be organized into logical groups.

**Hello:** A message sent by a *Service Provider* when it joins a network; this message contains key information for the *Service Consumer*.

**Bye:** A message sent by a *Service Provider* when it leaves a network.

**SearchRequest:** A message sent by a *Service Consumer* searching for *Service Providers* by *Scopes* or *Endpoint Identifier*.

**SearchResponse:** A message sent by a *Service Provider* in response to a *SearchRequest*.

**Metadata:** Information about a *Service Provider*; includes, but is not limited to, human-readable names, model names, firmware version, and serial number.

**Ad hoc Mode:** An operational mode of discovery in which the *Hello*, *Bye* and *SearchRequest* messages are sent by using UDP/IP multicast.

**Managed Mode:** An operational mode of discovery in which the *Hello*, *Bye* and *SearchRequest* messages are sent unicast to a *Discovery Proxy*.

### Implicit discovery in Ad hoc Mode

In *Ad hoc Mode*, a *Service Provider* shall send a *Hello* when it joins the network, when its *Endpoint* information changes, or when its *Metadata* changes. A *Service Consumer* that is interested in the *Service Provider* may send a *SearchRequest* if the *Hello* does not contain a physical address.

> Notes:
> A physical address can be missing from a *Hello*, e.g. if the *Service Provider* multicasts to multiple network adapters.

A *Service Provider* shall not send a *Bye* as the *Ad hoc Mode* uses an unsecured channel, and most *Service Consumers* would ignore that message anyway.

```mermaid
sequenceDiagram
    Service Provider->>Service Consumer: Hello
    
    opt if physical addresses are missing
        Service Consumer->>Service Provider: SearchRequest
        Service Provider->>Service Consumer: SearchResponse
    end
```

### Explicit discovery in Ad hoc Mode

In *Ad hoc Mode*, a *Service Consumer* may send a *SearchRequest* to search for *Service Providers*.

```mermaid
sequenceDiagram
    Service Consumer->>Service Provider: SearchRequest
    Service Provider->>Service Consumer: SearchResponse
```

### Implicit discovery in Managed Mode

In *Managed Mode*, a *Service Consumer* may use the `ConsumerProxyService` of a *Discovery Proxy* to subscribe to *Hello* and *Bye*. The *Discovery Proxy* shall respond with a stream of `HelloAndByeResponse` messages.

> Notes:
> - `ConsumerProxyService` is a gRPC service
> - `ProxySubscribeHelloAndByeRequest` requests to receive a stream of `ProxyHelloAndByeResponse` messages
> - `ProxyHelloAndByeResponse` is used by the `ConsumerProxyService` to convey *Hello* and *Bye*

```mermaid
sequenceDiagram
    Service Consumer->>Discovery Proxy: ConsumerProxyService.SubscribeHelloAndBye(ProxySubscribeHelloAndByeRequest)
    Discovery Proxy-->>Service Consumer: ProxySubscribeHelloAndByeResponse
    Discovery Proxy-->>Service Consumer: ProxySubscribeHelloAndByeResponse
    Discovery Proxy-->>Service Consumer: ProxySubscribeHelloAndByeResponse     
```

### Explicit discovery in Managed Mode

In *Managed Mode*, a *Service Consumer* may use the `ConsumerProxyService` of a *Discovery Proxy* to search for *Service Providers* matching specific *Scopes* or *Endpoint identifier*.

> Notes:
> - `ConsumerProxyService` is a gRPC service
> - `ProxySearchRequest` and `ProxySearchResponse` are messages used by the `Discovery Proxy` to convey search request and response data

```mermaid
sequenceDiagram
    Service Consumer->>+Discovery Proxy: ConsumerProxyService.Search(ProxySearchRequest)
    Discovery Proxy-->>-Service Consumer: ProxySearchResponse
```

### Notification of Hello and Bye in Managed Mode

In *Managed Mode*, the *Service Provider* shall use the `ProviderProxyService` of a *Discovery Proxy* to establish a stream of `NotifyHelloAndByeRequest` messages.

> Notes:
> - `ProviderProxyService` is a gRPC service
> - `ProxyNotifyHelloAndByeRequest` is used by the `ProviderProxyService` to publish *Hello* and *Bye*

```mermaid
sequenceDiagram
    Service Provider->>+Discovery Proxy: ProviderProxyService.NotifyHelloAndBye(ProxyNotifyHelloAndByeRequest)
    Service Provider->>+Discovery Proxy: ProviderProxyService.NotifyHelloAndBye(ProxyNotifyHelloAndByeRequest)
    Service Provider->>+Discovery Proxy: ProviderProxyService.NotifyHelloAndBye(ProxyNotifyHelloAndByeRequest)
```

### Getting Endpoint information and Metadata

Once the physical address of a *Service Provider* is known, a *Service Consumer* may request *Endpoint* information and *Metadata* by using `MetadataService.GetMetadata()`.

> Notes:
> - `MetadataService` is a gRPC service
> - GetMetadata can be used to verify correctness of a *Hello* or *SearchRequest* conveyed over an unsecured channel in *Ad hoc Mode*

```mermaid
sequenceDiagram
    Service Consumer->>+Service Provider: MetadataService.GetMetadata(GetMetadataRequest)
    Service Provider-->>-Service Consumer: GetMetadataResponse
```