val protoConverter: Configuration by configurations.creating

plugins {
    java
}


val baseVersion = File(rootProject.projectDir, "./config/base_version.txt").readText()
val buildId: String? = System.getenv("CI_PIPELINE_IID")

// append build id if present, make sure releases don't have build ids!
val actualVersion = buildId?.let { "${baseVersion}.${buildId}" } ?: baseVersion
val actualGroup = "org.somda.sdc.protosdc"

var converterVersion by extra("0.0.3.1")


allprojects {

    group = actualGroup
    version = actualVersion

    repositories {
        mavenCentral()
        // behold, the place where the converter stuff lies!
        maven {
            url = uri("https://gitlab.com/api/v4/projects/29289262/packages/maven")
        }
    }
}

dependencies {
    protoConverter(group = "org.somda.sdc.protosdc_converter", name = "converter", version = converterVersion)
}

val schemaFolder = "./xml_schema"

val executeConverterTask = task("executeConverter", JavaExec::class) {
    main = "org.somda.protosdc_converter.converter.MainKt"
    classpath = protoConverter
    args = listOf(
        "${schemaFolder}/BICEPS_MessageModel.xsd",
        "--java-multi-files",
        "--import-prefix", "org/somda/protosdc/proto/model/biceps",
        "--proto-package", "org.somda.protosdc.proto.model.biceps",
        "--kotlin-package", "org.somda.protosdc.biceps.model",
        "--kotlin-proto-mapping-package", "org.somda.protosdc.proto.mapping",
        "--message-name-suffix", "Msg"
    )
}

val cleanGenerated = tasks.create<Delete>("cleanGenerated") {
    // remove all child directories in proto_out
    val protoFilesToDelete = File("proto_out").listFiles()?.toList() ?: emptyList()
    val kotlinFilesToDelete = File("kotlin_out").listFiles()?.toList() ?: emptyList()
    val mappingFilesToDelete = File("proto_kotlin_out").listFiles()?.toList() ?: emptyList()
    val rustFilesToDelete = File("rust_out").listFiles()?.toList() ?: emptyList()
    val rustMappingFilesToDelete = File("proto_rust_out").listFiles()?.toList() ?: emptyList()
    val filesToDelete = protoFilesToDelete + kotlinFilesToDelete + mappingFilesToDelete + rustFilesToDelete + rustMappingFilesToDelete
    println("Removing $filesToDelete")
    delete = filesToDelete.toSet()
}


// always clean any previous proto before generating new output
executeConverterTask.dependsOn(cleanGenerated)

// attach to normal gradle tasks
tasks.clean { dependsOn(cleanGenerated) }
tasks.assemble { dependsOn(executeConverterTask) }